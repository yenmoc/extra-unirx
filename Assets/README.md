# Extra UniRx

## Installation

```bash
"com.yenmoc.extra-unirx":"https://gitlab.com/yenmoc/extra-unirx"
or
npm publish --registry=http://localhost:4873
```

## Usages

```csharp
var subject = new Subject<int>();
var observer = new TestObserver<int>();

subject.Subscribe(observer);

subject.OnNext(1);
subject.OnNext(2);
subject.OnCompleted();

Assert.AreEqual(2, observer.OnNextCount);
Assert.AreEqual(1, observer.OnNextValues[0]);
Assert.AreEqual(2, observer.OnNextValues[1]);
Assert.AreEqual(1, observer.OnCompletedCount);
```

### SubjectProperty

SubjectProperty is like a ReactiveProperty, but it's Subject.
It's IObserver & IObservable, and not holding latest value in stream.

```csharp
var property = new SubjectProperty<int>();
property.Subscribe(observer); // behave as IObservable
observable.Subscribe(property); // behave as IObserver
property.Value; // and it's easy to access value
```

### IsDefault(), IsNotDefault()

IsDefault(), IsNotDefault() are operators for `IObservable<T>`.
These operators stream values when the streamed value is default (or not default).

```csharp
var intSubject = new Subject<int>();
intSubject.IsDefault().Subscribe(x => Debug.Log(x)); // 0
intSubject.IsNotDefault().Subscribe(x => Debug.Log(x)); // 100
intSubject.OnNext(0);
intSubject.OnNext(100);

var objectSubject = new Subject<AnyClass>();
objectSubject.IsDefault().Subscribe(x => Debug.Log(x)); // "null"
objectSubject.IsNotDefault().Subscribe(x => Debug.Log(x)); // "AnyClass"
objectSubject.OnNext(null);
objectSubject.OnNext(new AnyClass());
```

Both methods accept a selector as an argument to select values to determine.

```csharp
var intSubject = new Subject<int>();
intSubject.IsDefault(x => x - 100).Subscribe(x => Debug.Log(x)); // 0
intSubject.IsDefault(x => x - 100).Subscribe(x => Debug.Log(x)); // 0, 100
intSubject.IsNotDefault(x => x - 100).Subscribe(x => Debug.Log(x)); // 100
intSubject.OnNext(0);
intSubject.OnNext(100);

class AnyClass { bool Value; }

var objectSubject = new Subject<AnyClass>();
objectSubject.IsDefault().Subscribe(x => Debug.Log(x)); // null, "AnyClass"
objectSubject.IsDefault().Subscribe(x => Debug.Log(x.Value)); // throw NullReferenceException in Subscribe()
objectSubject.IsNotDefault().Subscribe(x => Debug.Log(x.Value)); // true
objectSubject.OnNext(null);
objectSubject.OnNext(new AnyClass() { Value = false });
objectSubject.OnNext(new AnyClass() { Value = true });
```
